import React from 'react';
import './App.css';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import GridItem from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import Divider from '@material-ui/core/Divider';
import CardContent from '@material-ui/core/CardContent';
import grey from '@material-ui/core/colors/grey';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';

class Register extends React.Component {

    constructor() {
        super();
        this.state = {
            firstName: '',
            secondName: '',
            email: '',
            password: ''

        };
    }

    handleFirstName = (event) => {
        this.setState({
            firstName: event.target.value
        });
    }


    handleSecondName = (event) => {
        this.setState({
            secondName: event.target.value
        });
    }


    handleEmail = (event) => {
        this.setState({
            email: event.target.value
        });
    }


    handlePassword = (event) => {
        this.setState({
            password: event.target.value
        });
    }

    register = (event) => {
        var data = JSON.parse(window.localStorage.getItem('userDetails'));
        data.push({ firstName: this.state.firstName, secondName: this.state.secondName, email: this.state.email, password: this.state.password });

        var userDetails = JSON.stringify(data);
        window.localStorage.setItem('userDetails', userDetails);
        event.preventDefault();

    }

    render() {
        return (
            <div>
                <Card plain className='logIn'>
                    <img src={'img/shop-logo2.png'} alt="..." />
                    <CardContent>
                        <h1>Register</h1>
                        <input type='text' placeholder='First Name' value={this.state.firstName} onChange={this.handleFirstName} />
                        <input type='text' placeholder='Second Name' value={this.state.secondName} onChange={this.handleSecondName} />
                        <input type='text' placeholder='Email' value={this.state.email} onChange={this.handleEmail} />
                        <input type='text' placeholder='First Name' />

                        <input type='password' placeholder='Password' value={this.state.password} onChange={this.handlePassword} />
                        <Button onClick={this.register}>Register</Button>
                    </CardContent>

                </Card>
            </div >
        );
    }
}

export default Register;