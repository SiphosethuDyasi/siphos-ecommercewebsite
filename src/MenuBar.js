import './App.css';
import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import grey from '@material-ui/core/colors/grey';
import Cart from './Cart';
import App from './App';

import Account from './Account';



class ButtonAppBar extends React.Component {

    constructor() {
        super()

    }

    render() {
        return (

            <AppBar position="fixed" elevation={0} className='colors' style={{ backgroundColor: "white" }}>
                <Toolbar>
                    <img src={'img/shop-logo2.png'} alt="..." />
                    <Typography variant="h5" className='title'></Typography>

                    <Button
                        color={grey[500]}
                        startIcon={<Avatar src={'img/cart-img.jpg'} />}> Cart
                            </Button>
                    <Button
                        color={grey[500]}
                        startIcon={<Avatar src={'img/account2.png'} />}>
                        Account
                            </Button>

                    <Button color={grey[500]}>Log out</Button>
                </Toolbar>
            </AppBar >


        );
    }
}

export default ButtonAppBar;

