import React from 'react';
import './App.css';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import GridItem from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import PopUpWindow from './PopUpWindow';

class Account extends React.Component {

    constructor() {
        super();
        this.state = {
            cartArray: JSON.parse(window.localStorage.getItem('checkoutArray')),
            productArray: JSON.parse(window.localStorage.getItem('products')),
            showItems: false,
            orderNum: 0,
        };

        this.showItems = this.showItems.bind(this);
        this.closeItems = this.closeItems.bind(this);
    }

    showItems = (orderNumber) => {

        this.setState({
            orderNum: orderNumber
        })
        this.setState({
            showItems: true
        });
    }

    getproduct() {
        var products = JSON.parse(window.localStorage.getItem('checkoutArray'));

        for (var i = 0; i < products.length; i++) {
            if (products[i].orderNumber === this.state.productIds) {
                <p>{JSON.parse(products[i].products)}</p>
            }
        }
    }

    closeItems() {
        this.setState({ showItems: false }
        );
    }

    getNumOfProducts(orderNumber) {
        var products = JSON.parse(window.localStorage.getItem('checkoutArray'));
        var items = 0;
        var p;
        var productArray = [];

        for (var i = 0; i < products.length; i++) {
            if (products[i].orderNumber === orderNumber) {
                productArray.push(products[i].products);
            }
        }

        p = JSON.parse(productArray);

        for (var x = 0; x < p.length; x++) {
            items += p[x].quantity;
        }
        return items;
    }


    render() {
        const { cartArray } = this.state;

        return (
            <Paper className='paper'>
                <h1>Hello Siphosethu</h1>
                <p>You can view all of your recent orders below and manage your account details</p>

                <Grid container spacing={2} className='grid'>
                    <GridItem xs={12} sm={2}>
                        <p>Order Number</p>
                    </GridItem>
                    <GridItem xs={12} sm={2}>
                        <p>Order Date</p>
                    </GridItem>

                    <GridItem xs={12} sm={2}>
                        <p>Delivery Date</p>
                    </GridItem>
                    <GridItem xs={12} sm={6}>
                        <p>Total</p>
                    </GridItem>
                </Grid>

                <Card elevation={0} style={{ width: '100%' }}>
                    <Divider variant="middle" />
                </Card>

                {
                    cartArray.map(item => (
                        <Grid key={item.orderNumber} container spacing={2} className='grid'>
                            <GridItem xs={12} sm={2}>
                                <p>{item.orderNumber}</p>
                            </GridItem>
                            <GridItem xs={12} sm={2}>
                                <p>{item.orderDate}</p>
                            </GridItem>
                            <GridItem xs={12} sm={2}>
                                <p>{item.deliveryDate}</p>
                            </GridItem>
                            <GridItem xs={12} sm={4}>
                                <p>Total Price is R {item.totalPrice}.00 for {this.getNumOfProducts(item.orderNumber)} items</p>
                            </GridItem>
                            <GridItem xs={12} sm={2}>
                                <Button onClick={() => this.showItems(item.orderNumber)}>
                                    View Products
                                </Button>
                            </GridItem>
                            {
                                this.state.showItems ? (
                                    <PopUpWindow
                                        text={this.state.orderNum}
                                        closePopup={this.closeItems}
                                    />
                                ) : (null)
                            }
                        </Grid>
                    ))
                }
            </Paper>
        )
    }
}

export default Account;