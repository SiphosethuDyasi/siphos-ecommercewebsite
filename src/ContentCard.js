import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import GridItem from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import deepPurple from '@material-ui/core/colors/deepPurple';
import React from 'react';
import './App.css';

class ContentCard extends React.Component {

    constructor() {
        super();
        this.state = {
            products: [
                {
                    id: 1,
                    title: 'Forever Love Ring',
                    price: 100.00,
                    image: 'img/ring1.png'
                },
                {
                    id: 2,
                    title: 'Silver Star Ring',
                    price: 600.00,
                    image: 'img/ring2.png'
                },
                {
                    id: 3,
                    title: 'Golden Hour Ring',
                    price: 300.00,
                    image: 'img/ring3.png'
                },
                {
                    id: 4,
                    title: 'Butterfly Studs',
                    price: 200.00,
                    image: 'img/bowEarrings.png'
                },
                {
                    id: 5,
                    title: 'Jeweled Dog Studs',
                    price: 200.00,
                    image: 'img/DoggyEarrings.png'
                },
                {
                    id: 6,
                    title: 'Infinity Heart Studs',
                    price: 400.00,
                    image: 'img/HeartEarrings.png'
                },
                {
                    id: 7,
                    title: 'Garden Necklace<',
                    price: 400.00,
                    image: 'img/necklace1.png'
                },
                {
                    id: 8,
                    title: 'Flower Necklace',
                    price: 400.00,
                    image: 'img/necklace2.png'
                },
                {
                    id: 9,
                    title: 'Butterfly Necklace',
                    price: 400.00,
                    image: 'img/necklace3.png'
                },
                {
                    id: 10,
                    title: 'Pearl Flower Bracelet',
                    price: 600.00,
                    image: 'img/bracelet1.png'
                },
                {
                    id: 11,
                    title: 'Saturn Bracelet',
                    price: 300.00,
                    image: 'img/bracelet2.png'
                },
                {
                    id: 12,
                    title: 'Glamour Chain Bracelet',
                    price: 700.00,
                    image: 'img/bracelet3.png'
                },
            ],
        };
    }

    componentDidMount() {
        var products = JSON.parse(window.localStorage.getItem('productArray'));
        if (products === null) {
            var array = [];
            window.localStorage.setItem('productArray', JSON.stringify(array));
        }
    }

    addToCart = (productId, img, name, price) => {
        var data = JSON.parse(window.localStorage.getItem('productArray'));
        data.push({ id: productId, title: name, price: price, image: img, quantity: 1 });
        var testObject = JSON.stringify(data);
        window.localStorage.setItem('productArray', testObject);
    }

    render() {
        const { products } = this.state;

        return (
            <Grid container spacing={3}>
                {products.map(product => (
                    < GridItem key={product.id} style={{ margin: 33 }} item xs={6} sm={3}>
                        <Card plain >
                            <img src={product.image} alt="..." className='productImg' />
                            <CardContent style={{
                                textAlign: 'center',
                                fontFamily: 'Montserrat'
                            }}>
                                <h3>{product.title}</h3>
                                <p>R {product.price}.00</p>

                                <Button onClick={() => this.addToCart(product.id, product.image, product.title, product.price)} style={{ color: deepPurple[400] }}>Add to Cart</Button>
                            </CardContent>
                        </Card>
                    </GridItem>
                ))
                }
            </Grid>
        );
    }
}

export default ContentCard;


