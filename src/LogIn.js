import React from 'react';
import './App.css';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';

class LogIn extends React.Component {

    constructor() {
        super();
        this.state = {
            email: '',
            password: ''
        };
    }

    handleEmail = (event) => {
        this.setState({
            email: event.target.value
        });
    }

    handlePassword = (event) => {
        this.setState({
            password: event.target.value
        });
    }

    logIn = () => {
        var data = JSON.parse(window.localStorage.getItem('userDetails'));

        for (var i = 0; i < data.length; i++) {
            if (data[i].email === this.state.email && data[i].password === this.state.password) {
                window.localStorage.setItem('token', 'loggedIn');

            }
        }
    }

    render() {
        return (
            <Paper className='paper'>
                <Card plain className='logIn'>
                    <CardContent>
                        <h1>Log In</h1>
                        <input type='text' placeholder='Email' value={this.state.email} onChange={this.handleEmail} />
                        <input type='password' placeholder='Password' value={this.state.password} onChange={this.handlePassword} />
                        <Button onClick={this.logIn}>Log In</Button>
                    </CardContent>

                </Card>
            </Paper >
        );
    }
}

export default LogIn;