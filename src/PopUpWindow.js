import React from 'react';
import './App.css';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import GridItem from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';


class PopUp extends React.Component {

    constructor() {
        super();
        this.state = {
            cartArray: JSON.parse(window.localStorage.getItem('checkoutArray')),
            products: JSON.parse(window.localStorage.getItem('products')),
            productArray: [],
            name: [],
            orderNum: 0
        };
    }

    componentDidMount() {
        var items = [];
        var d = [];
        var item2;

        for (var i = 0; i < this.state.cartArray.length; i++) {
            if (this.state.cartArray[i].orderNumber === this.props.text) {

                items.push(this.state.cartArray[i].products);
            }
        }
        item2 = JSON.parse(items);
        this.setState({
            productArray: item2
        });

        for (var u = 0; u < item2.length; u++) {
            for (var x = 0; x < this.state.products.length; x++) {
                if (item2[u].productId === this.state.products[x].id) {
                    d.push({ id: this.state.products[x].id, image: this.state.products[x].image, title: this.state.products[x].title, price: this.state.products[x].price, quantity: item2[i].quantity });
                }
            }
        }

        this.setState({
            name: d
        });
    }


    render() {
        return (
            <Paper className='paper'>
                < div className='popup' >
                    <div className='popup_inner'>
                        <Button onClick={this.props.closePopup}>Close Me</Button>
                        <Grid container spacing={1}>
                            <GridItem xs={12} sm={6}>
                                <p>Product </p>
                            </GridItem>
                            <GridItem xs={12} sm={3}>
                                <p>Price</p>
                            </GridItem>
                            <GridItem xs={12} sm={3}>
                                <p>Number of Items</p>
                            </GridItem>
                        </Grid>

                        {this.state.name.map(items => (
                            <Grid key={items.id} container spacing={1} className='grid'>
                                <GridItem xs={12} sm={3} className='gridheight'>
                                    <img src={items.image} alt='product' className='h' />
                                </GridItem>
                                <GridItem xs={12} sm={3} className='gridheight'>
                                    <p>{items.title}</p>
                                </GridItem>
                                <GridItem xs={12} sm={3} className='gridheight'>
                                    <p>{items.price}</p>
                                </GridItem>
                                <GridItem xs={12} sm={3} className='gridheight'>
                                    <p>{items.quantity}</p>
                                </GridItem>
                            </Grid>
                        ))}
                    </div>
                </div >
            </Paper>
        );
    }
}

export default PopUp