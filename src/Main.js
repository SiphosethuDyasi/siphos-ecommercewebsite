import './App.css';
import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from './Home';
import Cart from './Cart';
import Account from './Account';
import LogIn from './LogIn';


class Main extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <Router>
                <AppBar position="fixed" elevation={0} className='colors' style={{ backgroundColor: "white" }}>
                    <Toolbar>
                        <img src={'img/shop-logo2.png'} alt="..." />
                        <Typography variant="h5" className='title'></Typography>

                        <Button
                            // color={grey[500]}
                            startIcon={<Avatar src={'img/cart-img.jpg'} />}>
                            <Link to={'/'}>Home</Link>
                        </Button>
                        <Button
                            //color={grey[500]}
                            startIcon={<Avatar src={'img/cart-img.jpg'} />}>
                            <Link to={'/cart'}>Cart</Link>
                        </Button>
                        <Button
                            //color={grey[500]}
                            startIcon={<Avatar src={'img/account2.png'} />}>
                            <Link to={'/account'}>Account</Link>
                        </Button>
                        <Button //color={grey[500]}
                        >Log out</Button>
                    </Toolbar>
                </AppBar >
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route exact path='/login' component={LogIn} />
                    <Route exact path='/cart' component={Cart} />
                    <Route exact path='/account' component={Account} />
                </Switch>
            </Router>
        );
    }
}

export default Main;