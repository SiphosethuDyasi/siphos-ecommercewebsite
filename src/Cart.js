import React from 'react';
import './App.css';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';


class Cart extends React.Component {

    constructor() {
        super();
        this.state = {
            products: JSON.parse(window.localStorage.getItem('productArray')),
            checkoutArray: JSON.parse(window.localStorage.getItem('checkoutArray')),
            productQuantity: 1,
            totalPrice: 0,
            showMenu: false,
            menuText: 'Click to choose shipping',
            errorMessage: '',
            shippingID: 0,
            checkItems: false,
            menuItems: [
                {
                    id: 1,
                    itemName: 'Normal 10 business days (R 300.00)',
                    price: 300,
                    days: 10
                },
                {
                    id: 2,
                    itemName: 'Priority Delivery 3 business days (R 400.00)',
                    price: 400,
                    days: 3
                },
                {
                    id: 3,
                    itemName: 'Day Saver 2 business days (R 550.00)',
                    price: 550,
                    days: 2
                },
                {
                    id: 4,
                    itemName: 'Fast same day delivery (R 700.00)',
                    price: 700,
                    days: 1
                }

            ],
            productId: []
        };

        this.showMenu = this.showMenu.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
    }

    componentDidMount() {
        this.setState({
            totalPrice: this.getOverallTotal()
        });

        if (window.localStorage.getItem('checkoutArray') === null) {
            var array = [];
            window.localStorage.setItem('checkoutArray', JSON.stringify(array));
        }

        this.setState({
            checkoutArray: JSON.parse(window.localStorage.getItem('checkoutArray'))
        });

        if (this.state.products.length !== 0) {
            var array2 = [];
            this.setState({
                checkItems: true
            });
            for (var i = 0; i < this.state.products.length; i++) {
                array2.push({ productId: this.state.products[i].id, quantity: this.state.products[i].quantity });
            }
        } else {
            this.setState({
                checkItems: false
            });
        }

        this.setState({
            productId: array2
        });

    }

    showMenu(event) {
        event.preventDefault();
        this.setState({
            showMenu: true
        });
    }

    closeMenu = (displayText, shippingPrice, shippingid) => {
        this.setState({ showMenu: false }, () => {
            document.removeEventListener('click', this.closeMenu);

        });

        this.setState({
            shippingID: shippingid
        })

        this.setState({
            menuText: displayText
        })

        this.setState({
            totalPrice: this.getOverallTotal() + shippingPrice
        });

        this.setState({
            errorMessage: ''
        })
    }


    getOverallTotal() {
        var data = JSON.parse(window.localStorage.getItem('productArray'));
        var total = 0;
        for (var i = 0; i < data.length; i++) {
            total += data[i].quantity * data[i].price;
        }

        return (
            total
        )
    }

    handleSubtractQuantity = (productId) => {
        var data = JSON.parse(window.localStorage.getItem('productArray'));

        for (var i = 0; i < data.length; i++) {
            if (data[i].id === productId) {
                if (data[i].quantity > 1) {
                    data[i].quantity = data[i].quantity - 1;
                    window.localStorage.setItem('productArray', JSON.stringify(data));
                    this.setState({
                        products: JSON.parse(window.localStorage.getItem('productArray'))
                    })
                }
            }
        }
    }
    handleAddQuantity = (productId) => {
        var data = JSON.parse(window.localStorage.getItem('productArray'));

        for (var i = 0; i < data.length; i++) {
            if (data[i].id === productId) {
                data[i].quantity = data[i].quantity + 1;
                window.localStorage.setItem('productArray', JSON.stringify(data));
                this.setState({
                    products: JSON.parse(window.localStorage.getItem('prodctArray'))
                });
            }
        }
    }
    deleteItem = (productId) => {
        var data = JSON.parse(window.localStorage.getItem('productArray'));

        if (this.state.products.length !== 0) {
            this.setState({
                checkItems: true
            });
            for (var i = 0; i < data.length; i++) {
                if (data[i].id === productId) {
                    data.splice(i, 1);
                    var testObject = JSON.stringify(data);
                    window.localStorage.setItem('productArray', testObject);

                    this.setState({
                        products: JSON.parse(window.localStorage.getItem('productArray'))
                    });
                }
            }
        } else {
            this.setState({
                checkItems: false
            });
        }
    }


    checkOut = () => {

        var menuArray = this.state.menuItems;
        var shippingInfo, shippingPrice;
        var date = new Date();
        var day, month, year, deliveryDate, deliveryDays;

        if (this.state.shippingID !== 0) {
            for (var i = 0; i < menuArray.length; i++) {
                if (menuArray[i].id === this.state.shippingID) {
                    shippingInfo = menuArray[i].itemName;
                    shippingPrice = menuArray[i].price;
                    deliveryDays = menuArray[i].days;
                    day = date.getDate() + deliveryDays;
                    month = date.getMonth() + 1;
                    year = date.getFullYear();
                    deliveryDate = day + '/' + month + '/' + year;

                    var orderNum = Math.round(Math.random() * (300 - 100) + 100);
                    this.state.checkoutArray.push({ orderNumber: orderNum, products: JSON.stringify(this.state.productId), shippinginfo: shippingInfo, shippingPrice: shippingPrice, totalPrice: this.state.totalPrice, orderDate: date.toLocaleDateString(), deliveryDate: deliveryDate });
                    var check = JSON.stringify(this.state.checkoutArray);
                    window.localStorage.setItem('checkoutArray', check);
                }
            }
        } else {
            this.setState({
                errorMessage: 'You need to pick shipping amount before checking out'
            })
        }
    }
    render() {
        const { products } = this.state;
        const { menuItems } = this.state;

        return (
            < Paper className='paper'>
                <Grid container spacing={1} className='grid'>
                    <Grid item xs={12} sm={4}>
                        <h3>Shopping Cart</h3>
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <h3>Number of Items</h3>
                    </Grid>
                    <Grid item xs={12} sm={4} style={{ textAlign: 'center' }}>
                        <h3> Order Summary</h3>
                    </Grid>
                </Grid>

                <Card elevation={0} style={{ width: '100%' }}>
                    <Divider variant="middle" />
                </Card>

                {
                    this.state.checkItems ?
                        <div class='main-grid'>

                            <div class='item1'>
                                <div class="grid-container">
                                    <div class="productDescription">Product Description</div>
                                    <div class="quantity">Quantity</div>
                                    <div class="price">Price</div>
                                    <div class="total">Total</div>
                                    <div class='deleteItem'>Delete</div>
                                </div>
                                {products.map(product => (
                                    <div key={product.id} class='team2'>
                                        <div class="img"><img src={product.image} className='productImg' alt='product' /></div>
                                        <div class="des"><p>{product.title}</p></div>
                                        <div class="quant">
                                            <Button
                                                onClick={() => this.handleSubtractQuantity(product.id)}>-</Button>
                                            <label>{product.quantity}</label>
                                            <Button
                                                onClick={() => this.handleAddQuantity(product.id)}>+</Button>
                                        </div>
                                        <div class="pri"><p>{product.price}</p></div>
                                        <div class="t"><p>{product.price * product.quantity}</p></div>
                                        <div class='delete'>
                                            <Button
                                                onClick={() => this.deleteItem(product.id)}
                                                startIcon={<Avatar src={'img/deleteIcon.png'} />}>
                                            </Button>
                                        </div>
                                    </div>
                                ))}
                            </div>

                            <div class='item2'>
                                <form>
                                    <div class='grid-container2'>
                                        <div class="numofitems"><p>ITEMS: {products.length}</p></div>
                                        <div class="overalltotal"><p>{this.getOverallTotal()}</p></div>
                                        <div class="shipping"><p>SHIPPING</p> <p>{this.state.errorMessage}</p></div>
                                        <div class="dropdown">
                                            <Button onClick={this.showMenu}>
                                                {this.state.menuText}
                                            </Button>

                                            {
                                                this.state.showMenu ? (
                                                    menuItems.map(item => (
                                                        <div key={item.id} className='menu'>
                                                            <Button onClick={() => this.closeMenu(item.itemName, item.price, item.id)}>{item.itemName}</Button>
                                                        </div>
                                                    ))
                                                ) : (null)
                                            }
                                        </div>
                                        <div class='divider'>
                                            <Card elevation={0} style={{ width: '100%' }}>
                                                <Divider variant="middle" />
                                            </Card>
                                        </div>
                                        <div class="theading"><p>TOTAL COST</p></div>
                                        <div class="money"><p>{this.state.totalPrice}</p></div>
                                        <div class="checkoutbutton"><Button onClick={() => this.checkOut()}>CHECKOUT</Button></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        : <h1>No items</h1>
                }

            </Paper >
        );
    }
}
export default Cart;