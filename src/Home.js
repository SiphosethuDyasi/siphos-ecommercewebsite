import './App.css';
import React from 'react';
import ContentCard from './ContentCard';
import { Paper } from '@material-ui/core';

class Home extends React.Component {
    render() {
        return (
            <Paper className='paper'>
                <div className='divStyle'>
                    <ContentCard />
                </div>
            </Paper>
        );
    }
}

export default Home;