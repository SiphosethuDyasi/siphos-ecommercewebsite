const imageStyles = {
    imgFluid: {
        maxWidth: "100%",
        height: "auto"
      },
      imgRounded: {
        borderRadius: "6px !important"
      },
      imgRoundedCircle: {
        borderRadius: "50% !important"
      }
}